# MovieFinder
[![IDE](https://img.shields.io/badge/Xcode-11-blue.svg)](https://developer.apple.com/xcode/)
[![Language](https://img.shields.io/badge/swift-5-orange.svg)](https://swift.org)
[![Platform](https://img.shields.io/badge/iOS-10-green.svg)](https://developer.apple.com/ios/)

A sample iOS app built using the Clean Swift architecture and The Movie DB API.
https://clean-swift.com
https://www.themoviedb.org/
