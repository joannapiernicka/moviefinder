//
//  MovieRouter.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Alamofire

enum MovieApiRouter: URLRequestConvertible {
    case getNowPlaying(request: MovieListRequest)
    case getMovieDetails(postId: Int)
    case searchMovie(request: SearchRequest)


    func asURLRequest() throws -> URLRequest {
        let url = try API.baseURL.asURL()

        var urlRequest = URLRequest(url: url.appendingPathComponent(path))

        urlRequest.httpMethod = method.rawValue

        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")

        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()

        urlRequest = try encoding.encode(urlRequest, with: parameters)

        return urlRequest
    }

    private var method: HTTPMethod {
        switch self {
        case .getNowPlaying, .getMovieDetails, .searchMovie:
            return .get
        }
    }

    private var path: String {
        switch self {
        case .getNowPlaying:
            return "/movie/now_playing"
        case .getMovieDetails(let id):
            return "/movie/\(id)"
        case .searchMovie:
            return "/search/movie"
        }
    }

    private var parameters: Parameters? {
        switch self {
        case .getNowPlaying(let request):
            return Mapper.encodeToJSON(request)
        case .getMovieDetails:
            return nil
        case .searchMovie(let request):
            return Mapper.encodeToJSON(request)
        }
    }
}
