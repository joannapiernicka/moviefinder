//
//  AuthorizationAdapter.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Alamofire

final class AuthorizationAdapter: RequestAdapter, RequestInterceptor {

    // MARK: RequestAdapter

    func adapt(_ urlRequest: URLRequest,
               for session: Session,
               completion: @escaping (Result<URLRequest, Error>) -> Void) {
        let authorizationToken = "" //TODO: get sessionId from Keychain
        var urlRequest = urlRequest
        urlRequest.setValue("Bearer \(authorizationToken)", forHTTPHeaderField: "Authorization")

        completion(.success(urlRequest))
    }
}
