//
//  RestRepository.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Alamofire

class RestRepository {
    typealias REST = RestRepository

    static let auth = getAuthSessionManager()
    static let open = Session()

    func url(_ url: String) -> String {
        let encoded = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        return API.baseURL + encoded
    }

    private static func getAuthSessionManager() -> Session {
        let manager = Session(interceptor: AuthorizationAdapter())
        return manager
    }
}
