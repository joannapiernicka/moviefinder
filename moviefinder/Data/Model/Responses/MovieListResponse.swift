//
//  MovieListResponse.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Foundation

struct MovieListResponse: Codable {
    let page: Int
    let results: [Movie]
    let dates: DateRange?
    let total_pages: Int
    let total_results: Int
}

struct DateRange: Codable {
    let maximum: Date
    let minimum: Date
}

