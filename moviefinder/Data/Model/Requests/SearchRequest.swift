//
//  SearchRequest.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Foundation

struct SearchRequest: Codable {
    let api_key: String = API.key
    let query: String
    let page: Int
}
