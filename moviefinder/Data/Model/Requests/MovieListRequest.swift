//
//  MovieListRequest.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Foundation

struct MovieListRequest: Codable {
    let api_key: String = API.key
    let language: String = Locale.preferredLanguages.first ?? "en-US"
    let page: Int
    let region: String = Locale.current.regionCode ?? "US"
}
