//
//  Movie.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Foundation

struct Movie: Codable {
    let posterPath: String?
    let isForAdult: Bool?
    let overview: String?
    let releaseDate: Date?
    let genreIds: [Int]?
    let id: Int
    let originalTitle: String?
    let originalLanguage: String?
    let title: String?
    let backdropPath: String?
    let popularity: Double?
    let voteCount: Int?
    let isVideo: Bool?
    let voteAverage: Double?

    var isFavorite: Bool = false

    enum CodingKeys: String, CodingKey {

        case posterPath = "poster_path"
        case isForAdult = "adult"
        case overview = "overview"
        case releaseDate = "release_date"
        case genreIds = "genre_ids"
        case id = "id"
        case originalTitle = "original_title"
        case originalLanguage = "original_language"
        case title = "title"
        case backdropPath = "backdrop_path"
        case popularity = "popularity"
        case voteCount = "vote_count"
        case isVideo = "video"
        case voteAverage = "vote_average"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        posterPath = try? values.decodeIfPresent(String.self, forKey: .posterPath)
        isForAdult = try? values.decodeIfPresent(Bool.self, forKey: .isForAdult)
        overview = try? values.decodeIfPresent(String.self, forKey: .overview)
        releaseDate = try? values.decodeIfPresent(Date.self, forKey: .releaseDate)
        genreIds = try? values.decodeIfPresent([Int].self, forKey: .genreIds)
        id = try (values.decodeIfPresent(Int.self, forKey: .id) ?? -1)
        originalTitle = try? values.decodeIfPresent(String.self, forKey: .originalTitle)
        originalLanguage = try? values.decodeIfPresent(String.self, forKey: .originalLanguage)
        title = try? values.decodeIfPresent(String.self, forKey: .title)
        backdropPath = try? values.decodeIfPresent(String.self, forKey: .backdropPath)
        popularity = try? values.decodeIfPresent(Double.self, forKey: .popularity)
        voteCount = try? values.decodeIfPresent(Int.self, forKey: .voteCount)
        isVideo = try? values.decodeIfPresent(Bool.self, forKey: .isVideo)
        voteAverage = try? values.decodeIfPresent(Double.self, forKey: .voteAverage)
    }
}
