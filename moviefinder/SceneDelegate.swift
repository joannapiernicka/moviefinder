//
//  SceneDelegate.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 19/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            guard let viewController = UIStoryboard(name: "MovieList", bundle: nil).instantiateInitialViewController() as? MovieListViewController,
                let listModule = MovieListConfigurator().configureModule(with: viewController) else {
                return
            }
            let navController = UINavigationController(rootViewController: listModule)
            window = UIWindow(windowScene: windowScene)
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
        }
    }
}

