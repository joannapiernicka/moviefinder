//
//  Mapper.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Foundation

enum Mapper {
    static func encodeToJSON<Type: Encodable>(_ value: Type) -> [String: Any]? {
        let encoder = JSONEncoder()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        encoder.dateEncodingStrategy = .formatted(dateFormatter)
        guard let encoded = try? encoder.encode(value) else {
            return nil
        }
        guard let json = try? JSONSerialization.jsonObject(with: encoded, options: .allowFragments) else {
            return nil
        }
        return json as? [String: Any]
    }

    static func decode<Type: Decodable>(_ data: Data) throws -> Type {
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return try decoder.decode(Type.self, from: data)
    }
}
