//
//  UIAlertController.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import UIKit

extension UIAlertController {
    class func dismissableError(message: String?,
                                title: String = "Oops!",
                                dismissButtonTitle: String = "OK",
                                handler: (() -> Void)? = nil) -> UIAlertController {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: dismissButtonTitle,
                                          style: .default,
                                          handler: { _ in
                                            handler?()
        })
        alertController.addAction(dismissAction)
        return alertController
    }
}
