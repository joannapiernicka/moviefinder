//
//  ApiService.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Alamofire

final class ApiService: ApiServiceType {}

protocol ApiServiceType: AnyObject {
    func request<T: Codable>(type: T.Type,
                             auth: Bool,
                             urlConvertible: URLRequestConvertible,
                             completion: ((Swift.Result<T, Error>) -> Void)?)
}

extension ApiServiceType {
    func request<T: Codable>(type: T.Type,
                             auth: Bool,
                             urlConvertible: URLRequestConvertible,
                             completion: ((Swift.Result<T, Error>) -> Void)?) {

        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }

        let restRepository = auth ? RestRepository.auth : RestRepository.open
        restRepository.request(urlConvertible).validate().responseData { (response: DataResponse<Data, AFError>) in

            UIApplication.shared.isNetworkActivityIndicatorVisible = false

            if let error = self.handle(responseStatusCode: response.response?.statusCode, error: response.error) {

                if let respData = response.data {
                    print("⚠️ error body \(String(describing: String(data: respData, encoding: .utf8)))")
                }

                completion?(.failure(error))
                return
            }

            switch response.result {
            case .success(let data):

                do {
                    let result: T = try Mapper.decode(data)
                    completion?(.success(result))
                    return
                } catch {
                    let json = try? JSONSerialization.jsonObject(with: data,
                                                    options: JSONSerialization.ReadingOptions.allowFragments)
                    print("⚠️ incompatible model, json input: \(String(describing: json))")
                }

            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }

    private func handle(responseStatusCode: Int?, error: AFError?) -> AFError? {
        let statusCode = responseStatusCode ?? 0

        switch statusCode {
        case 200...299:
            return nil
        default:
            //TODO: error handling
            return error
        }
    }

}
