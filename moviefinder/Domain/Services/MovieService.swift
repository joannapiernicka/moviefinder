//
//  MovieService.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Foundation

protocol MovieServiceType {
    func getNowPlaying(request: MovieListRequest,
                       completion: ((Result<MovieListResponse, Error>) -> Void)?)
    func searchMovie(request: SearchRequest,
                     completion: ((Result<MovieListResponse, Error>) -> Void)?)
}

final class MovieService: MovieServiceType {
    private var apiService: ApiServiceType

    init(apiService: ApiServiceType = ApiService()) {
        self.apiService = apiService
    }

    func getNowPlaying(request: MovieListRequest, completion: ((Result<MovieListResponse, Error>) -> Void)?) {
        apiService.request(type: MovieListResponse.self,
                           auth: false,
                           urlConvertible: MovieApiRouter.getNowPlaying(request: request),
                           completion: completion)
    }

    func searchMovie(request: SearchRequest, completion: ((Result<MovieListResponse, Error>) -> Void)?) {
        apiService.request(type: MovieListResponse.self,
                           auth: false,
                           urlConvertible: MovieApiRouter.searchMovie(request: request),
                           completion: completion)
    }
}
