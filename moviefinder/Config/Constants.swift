//
//  Constants.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Foundation

enum API {
    static var baseURL: String = "https://api.themoviedb.org/3"
    static let key = "aba64caceb342214701c675e6442474e"
    static let imagesSecureBaseURL: String = "https://image.tmdb.org/t/p/"

    enum PosterSize: String {
        case w92
        case w154
        case w185
        case w342
        case w500
        case w780
        case original
    }
}

