//
//  UserDefaultsWorker.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 21/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Foundation

protocol UserDefaultsWorkerType {

    ///List of favorite movies ids
    var favoriteMovies: [Int] { get set }
}

final class UserDefaultsWorker: UserDefaultsWorkerType {
    private let preferences = UserDefaults.standard

    var favoriteMovies: [Int] {
        get {
            return preferences.object(forKey: "favoriteMovies") as? [Int] ?? []
        }
        set {
            preferences.set(newValue, forKey: "favoriteMovies")
        }
    }
}
