//
//  AppDelegate.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 19/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        setupRootViewController()
        return true
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }

    private func setupRootViewController() {
        guard let viewController = UIStoryboard(name: "MovieList", bundle: nil).instantiateInitialViewController() as? MovieListViewController,
            let listModule = MovieListConfigurator().configureModule(with: viewController) else {
            return
        }
        let navController = UINavigationController(rootViewController: listModule)
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
}
