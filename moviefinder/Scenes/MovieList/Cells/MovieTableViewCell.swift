//
//  MovieTableViewCell.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import UIKit
import Kingfisher

protocol MovieDelegate: AnyObject  {
    func didChangeFavoriteState(movie: Movie)
}

final class MovieTableViewCell: UITableViewCell {

    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var posterImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var favoriteButton: UIButton!

    private weak var delegate: MovieDelegate?
    private var model: Movie?

    func configure(with movie: Movie, delegate: MovieDelegate?) {
        self.model = movie
        self.delegate = delegate
        if let posterPath = movie.posterPath {
            let url = URL(string: API.imagesSecureBaseURL+API.PosterSize.w500.rawValue+"/"+posterPath)
            posterImageView.kf.setImage(with: url)
        }
        titleLabel.text = movie.title
        favoriteButton.isSelected = movie.isFavorite
        favoriteButton.addTarget(self, action: #selector(didTapFavorite), for: .touchUpInside)
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        posterImageView.layer.cornerRadius = 8
        containerView.layer.cornerRadius = 8
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        containerView.layer.applyShadow()
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        posterImageView.image = nil
    }

    @objc func didTapFavorite() {
        favoriteButton.isSelected.toggle()
        model?.isFavorite = favoriteButton.isSelected
        guard let model = model else {
            return
        }
        delegate?.didChangeFavoriteState(movie: model)
    }

    func popScaleAnim(selected: Bool) {
        UIView.animate(withDuration: 0.2, animations: {
            let scale = selected ? 1.05 : 0.95 as CGFloat
            self.transform = CGAffineTransform(scaleX: scale, y: scale)
        }) { _ in
            UIView.animate(withDuration: 0.2, animations: {
                self.transform = CGAffineTransform(scaleX: 1, y: 1)
            })
        }
    }
}
