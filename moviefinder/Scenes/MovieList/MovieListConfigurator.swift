//
//  MovieListConfigurator.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import UIKit

protocol MovieListConfiguratorType {
    func configureModule(with viewController: MovieListViewController?) -> MovieListViewController?
}

final class MovieListConfigurator: MovieListConfiguratorType {
    func configureModule(with viewController: MovieListViewController?) -> MovieListViewController? {
        let presenter = MovieListPresenter(viewController: viewController)
        let interactor = MovieListInteractor(presenter: presenter)
        let router = MovieListRouter(viewController: viewController)
        
        viewController?.interactor = interactor
        viewController?.router = router
        
        return viewController
    }
}
