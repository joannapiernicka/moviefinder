//
//  MovieListPresenter.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Foundation

protocol MovieListPresenterType {
    func prepare(error: Error)
    func prepare(moviesList: [Movie])
    func prepareDetails(movie: Movie)
    func prepareSearch(result: [Movie], searchText: String)
}

final class MovieListPresenter {
    private weak var viewController: MovieListViewControllerType?
    
    init(viewController: MovieListViewControllerType?) {
        self.viewController = viewController
    }
}

extension MovieListPresenter: MovieListPresenterType {
    func prepare(error: Error) {
        viewController?.presentAlert(with: error.asAFError?.localizedDescription ?? "Something went wrong")
    }

    func prepare(moviesList: [Movie]) {
        viewController?.present(movies: moviesList)
    }

    func prepareDetails(movie: Movie) {
        viewController?.presentDetails(movie: movie)
    }

    func prepareSearch(result: [Movie], searchText: String) {
        viewController?.displaySearch(result: result, searchText: searchText)
    }
}
