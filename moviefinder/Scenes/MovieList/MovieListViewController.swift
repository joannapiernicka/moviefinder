//
//  MovieListViewController.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import UIKit

protocol MovieListViewControllerType: AnyObject {
    func presentAlert(with message: String)
    func present(movies: [Movie])
    func presentDetails(movie: Movie)
    func displaySearch(result: [Movie], searchText: String)
}

final class MovieListViewController: UIViewController {
    var interactor: MovieListInteractorType?
    var router: MovieListRouterType?

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var resultsContainer: UIView!

    private var autocompleteResultsViewController: AutocompleteResultsViewController?
    private let searchController = UISearchController(searchResultsController: nil)
    private let dataSource = MovieListDataSource()
    private var nextPage = 1

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        setupSearch()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        interactor?.getNowPlayingMovies()
    }

    private func setupTableView() {
        let nib = UINib(nibName: "MovieTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "MovieTableViewCell")
        dataSource.delegate = interactor
        tableView.dataSource = dataSource
        tableView.delegate = self
    }

    func setupSearch() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.delegate = self
        searchController.hidesNavigationBarDuringPresentation = false
        navigationItem.titleView = searchController.searchBar
        definesPresentationContext = true
        let searchResultVC = self.children.first { (viewController) -> Bool in
            viewController is AutocompleteResultsViewController
            } as? AutocompleteResultsViewController
        searchResultVC?.delegate = self
    }
}

extension MovieListViewController: AutocompleteResultsDelegate {
    func resultsController(didAutocompleteWith movie: Movie) {
        resultsContainer.isHidden = true
        searchController.searchBar.text = nil
        interactor?.didSelectMovie(model: movie)
    }
}

extension MovieListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        searchController.searchBar.setShowsCancelButton(false, animated: false)
    }

    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
}

extension MovieListViewController: UISearchControllerDelegate {
    func didPresentSearchController(_ searchController: UISearchController) {
        searchController.searchBar.setShowsCancelButton(false, animated: false)
    }
}

extension MovieListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            searchBar.resignFirstResponder()
            resultsContainer.isHidden = true
        } else {
            interactor?.searchMovie(searchText: searchText)
            resultsContainer.isHidden = false
            self.autocompleteResultsViewController?.tableView.reloadData()

        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard !searchBarIsEmpty() else {
            return
        }
        let searchResultVC = children.first.flatMap { $0 as? AutocompleteResultsViewController }
        searchResultVC?.cleanSearchResult()
        resultsContainer.isHidden = false
        interactor?.searchMovie(searchText: searchBar.text ?? "")
    }
}

extension MovieListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? MovieTableViewCell else {
            return
        }
        cell.popScaleAnim(selected: true)
        interactor?.didSelectMovie(index: indexPath.row)
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == dataSource.movies.count - 1 {
            interactor?.getNowPlayingMovies()
        }
    }
}

extension MovieListViewController: MovieListViewControllerType {
    func presentAlert(with message: String) {
        router?.presentAlert(with: message)
    }

    func present(movies: [Movie]) {
        dataSource.movies = movies
        tableView.reloadData()
    }

    func presentDetails(movie: Movie) {
        router?.navigateToDetails(movie: movie)
    }

    func displaySearch(result: [Movie], searchText: String) {
        resultsContainer.isHidden = result.isEmpty
        let searchResultVC = children.first.flatMap { $0 as? AutocompleteResultsViewController }
        searchResultVC?.searchTerm = searchText
        searchResultVC?.showSearchResult(movies: result)
    }
}
