//
//  MovieListDataSource.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import UIKit

final class MovieListDataSource: NSObject {
    var movies: [Movie] = []

    weak var delegate: MovieDelegate?

    func item(forIndexPath indexPath: IndexPath) -> Movie? {
        guard indexPath.row < movies.count else {
            return nil
        }
        return movies[indexPath.row]
    }
}

extension MovieListDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath) as? MovieTableViewCell,
            let movie = item(forIndexPath: indexPath) else {
            return UITableViewCell()
        }
        cell.configure(with: movie, delegate: delegate)

        return cell
    }
}
