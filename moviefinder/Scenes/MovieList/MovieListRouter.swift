//
//  MovieListRouter.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import UIKit

protocol MovieListRouterType {
    func presentAlert(with message: String)
    func navigateToDetails(movie: Movie)
}

final class MovieListRouter {
    weak var viewController: MovieListViewController?
    
    var navigationController: UINavigationController? {
        return viewController?.navigationController
    }
    
    init(viewController: MovieListViewController?) {
        self.viewController = viewController
    }
}

extension MovieListRouter: MovieListRouterType {
    func presentAlert(with message: String) {
        let alert = UIAlertController.dismissableError(message: message)
        viewController?.present(alert, animated: true, completion: nil)
    }

    func navigateToDetails(movie: Movie) {
        guard let viewController = UIStoryboard(name: "MovieDetails", bundle: nil).instantiateInitialViewController() as? MovieDetailsViewController,
            let detailsModule = MovieDetailsConfigurator().configureModule(with: viewController) else {
            return
        }
        detailsModule.movie = movie
        self.viewController?.present(detailsModule, animated: true, completion: nil)
    }
}
