//
//  MovieListInteractor.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Foundation

protocol MovieListInteractorType: MovieDelegate {
    func getNowPlayingMovies()
    func searchMovie(searchText: String)
    func didSelectMovie(index: Int)
    func didSelectMovie(model: Movie)
    func didChangeFavoriteState(movie: Movie)
}

final class MovieListInteractor {
    var presenter: MovieListPresenterType?

    private let service: MovieServiceType?
    private var worker: UserDefaultsWorkerType

    private var nextPage = 1
    private var totalPages = 0
    private var movies: [Movie] = [] {
        didSet {
            let updatedMovies = movies.compactMap { self.setFavoriteState(movie: $0) }
            self.movies = updatedMovies
        }
    }
    private var favoriteMoviesIDs: [Int] = []
    
    init(presenter: MovieListPresenterType,
         service: MovieServiceType = MovieService(),
         worker: UserDefaultsWorkerType = UserDefaultsWorker()) {
        self.presenter = presenter
        self.service = service
        self.worker = worker
    }

    private func fetchListFavoriteMoviesId() {
        self.favoriteMoviesIDs = worker.favoriteMovies
        let updatedMovies = movies.compactMap { self.setFavoriteState(movie: $0) }
        self.movies = updatedMovies
    }

    fileprivate func setFavoriteState(movie: Movie) -> Movie {
        var updatedMovie = movie
        updatedMovie.isFavorite = favoriteMoviesIDs.contains(movie.id)
        return updatedMovie
    }
}

extension MovieListInteractor: MovieDelegate {
    func didChangeFavoriteState(movie: Movie) {
        if movie.isFavorite {
            favoriteMoviesIDs.append(movie.id)
        } else {
            favoriteMoviesIDs.removeAll { $0 == movie.id }
        }
        worker.favoriteMovies = favoriteMoviesIDs
        guard let index = movies.firstIndex(where: { $0.id == movie.id}) else {
            return
        }
        movies[index].isFavorite = movie.isFavorite
    }
}

extension MovieListInteractor: MovieListInteractorType {
    func getNowPlayingMovies() {
        fetchListFavoriteMoviesId()

        let request = MovieListRequest(page: nextPage)
        service?.getNowPlaying(request: request, completion: { [weak self] (result: Result<MovieListResponse, Error>) in
            switch result {
            case .success(let response):
                guard let self = self else {
                    return
                }
                self.totalPages = response.total_pages
                if response.total_pages > 1 {
                    self.nextPage += 1
                }

                var newMovies = self.movies
                newMovies.append(contentsOf: response.results)
                self.movies = newMovies
                self.presenter?.prepare(moviesList: self.movies)
            case .failure(let error):
                self?.presenter?.prepare(error: error)
            }
        })
    }

    func searchMovie(searchText: String) {
        let request = SearchRequest(query: searchText, page: 1)
        service?.searchMovie(request: request, completion:{ [weak self] (result: Result<MovieListResponse, Error>) in
            switch result {
            case .success(let response):
                self?.presenter?.prepareSearch(result: response.results, searchText: searchText)
            case .failure(let error):
                self?.presenter?.prepare(error: error)
            }
        })
    }

    func didSelectMovie(index: Int) {
        guard index < movies.count else {
            return
        }
        let movie = movies[index]
        presenter?.prepareDetails(movie: movie)
    }

    func didSelectMovie(model: Movie) {
        let movie = setFavoriteState(movie: model)
        presenter?.prepareDetails(movie: movie)
    }
}
