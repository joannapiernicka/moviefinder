//
//  MovieDetailsRouter.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import UIKit

protocol MovieDetailsRouterType {
    
}

final class MovieDetailsRouter {
    weak var viewController: MovieDetailsViewController?
    
    var navigationController: UINavigationController? {
        return viewController?.navigationController
    }
    
    init(viewController: MovieDetailsViewController?) {
        self.viewController = viewController
    }

}

extension MovieDetailsRouter: MovieDetailsRouterType {
    
}
