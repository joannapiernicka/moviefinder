//
//  MovieDetailsViewController.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import UIKit
import Cosmos

protocol MovieDetailsViewControllerType: AnyObject {
    
}

final class MovieDetailsViewController: UIViewController {
    var interactor: MovieDetailsInteractorType?
    var router: MovieDetailsRouterType?
    var movie: Movie?

    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var ratingView: CosmosView!
    @IBOutlet private weak var ratingLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var favoriteButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let movie = movie {
            setupModel(movie: movie)
        }
    }

    private func setupModel(movie: Movie) {
        if let posterPath = movie.posterPath {
            let url = URL(string: API.imagesSecureBaseURL+API.PosterSize.w780.rawValue+"/"+posterPath)
            imageView.kf.setImage(with: url)
        }
        titleLabel.text = movie.title
        favoriteButton.isSelected = movie.isFavorite
        if let releaseDate = movie.releaseDate {
            let formatter = DateFormatter()
            formatter.dateStyle = .short
            formatter.timeZone = TimeZone.autoupdatingCurrent
            dateLabel.text = formatter.string(from: releaseDate)
        }
        ratingView.rating = movie.voteAverage ?? 0.0
        ratingLabel.text = "\(movie.voteAverage ?? 0.0)"
        descriptionLabel.text = movie.overview
    }


    @IBAction func favoriteAction(_ sender: UIButton) {
        sender.isSelected.toggle()
        movie?.isFavorite = sender.isSelected
        guard let movie = movie else {
            return
        }
        interactor?.didTapFavorite(movie: movie)
    }


    @IBAction func closeAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

}

extension MovieDetailsViewController: MovieDetailsViewControllerType {
    
}
