//
//  MovieDetailsInteractor.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Foundation

protocol MovieDetailsInteractorType {
    func didTapFavorite(movie: Movie)
}

final class MovieDetailsInteractor {
    var presenter: MovieDetailsPresenterType?

    private var worker: UserDefaultsWorkerType
    private var favoriteMoviesIDs: [Int] = []

    init(presenter: MovieDetailsPresenterType,
         worker: UserDefaultsWorkerType = UserDefaultsWorker()) {
        self.presenter = presenter
        self.worker = worker
        self.favoriteMoviesIDs = worker.favoriteMovies
    }
}

extension MovieDetailsInteractor: MovieDetailsInteractorType {
    func didTapFavorite(movie: Movie) {
        if movie.isFavorite {
            favoriteMoviesIDs.append(movie.id)
        } else {
            favoriteMoviesIDs.removeAll { $0 == movie.id }
        }
        worker.favoriteMovies = favoriteMoviesIDs
    }
}
