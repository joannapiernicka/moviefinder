//
//  MovieDetailsPresenter.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import Foundation

protocol MovieDetailsPresenterType {
    
}

final class MovieDetailsPresenter {
    private weak var viewController: MovieDetailsViewControllerType?
    
    init(viewController: MovieDetailsViewControllerType?) {
        self.viewController = viewController
    }
    
}

extension MovieDetailsPresenter: MovieDetailsPresenterType {
    
}
