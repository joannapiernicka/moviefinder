//
//  MovieDetailsConfigurator.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import UIKit

protocol MovieDetailsConfiguratorType {
    func configureModule(with viewController: MovieDetailsViewController?) -> MovieDetailsViewController?
}

final class MovieDetailsConfigurator: MovieDetailsConfiguratorType {
    func configureModule(with viewController: MovieDetailsViewController?) -> MovieDetailsViewController? {
        let presenter = MovieDetailsPresenter(viewController: viewController)
        let interactor = MovieDetailsInteractor(presenter: presenter)
        let router = MovieDetailsRouter(viewController: viewController)
        
        viewController?.interactor = interactor
        viewController?.router = router
        
        return viewController
    }
}
