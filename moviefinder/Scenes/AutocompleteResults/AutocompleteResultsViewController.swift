//
//  AutocompleteResultsViewController.swift
//  moviefinder
//
//  Created by Joanna Piernicka on 20/07/2020.
//  Copyright © 2020 Joanna Piernicka. All rights reserved.
//

import UIKit

protocol AutocompleteResultsDelegate: AnyObject {
    func resultsController(didAutocompleteWith movie: Movie)
}

final class AutocompleteResultsViewController: UITableViewController {

    var items: [Movie] = []
    var searchTerm: String = ""
    weak var delegate: AutocompleteResultsDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(AutocompleteCell.self, forCellReuseIdentifier: AutocompleteCell.reuseIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
    }

    func item(forIndexPath indexPath: IndexPath) -> Movie? {
        guard indexPath.row < items.count, indexPath.row >= 0 else {
            return nil
        }
        return items[indexPath.row]
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let item = item(forIndexPath: indexPath) else {
            return UITableViewCell()
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: AutocompleteCell.reuseIdentifier, for: indexPath) as! AutocompleteCell
        cell.configure(searchTerm: searchTerm, item: item)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let movie = item(forIndexPath: indexPath) else {
            return
        }
        delegate?.resultsController(didAutocompleteWith: movie)
    }
}

extension AutocompleteResultsViewController {
    func showSearchResult(movies: [Movie]) {
        items = movies
        tableView.reloadData()
    }

    func cleanSearchResult() {
        items = []
        tableView.reloadData()
    }
}
